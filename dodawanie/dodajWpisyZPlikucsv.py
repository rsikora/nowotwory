import csv
import couchdb


serwer = couchdb.Server('http://quxer:wsadaq123@quxer.cloudant.com/')
bazadanych = serwer['europa']


def dodajwpis(csvnazwa):
    with open('static/'+csvnazwa, 'rb') as csvplik:
        czytnik = csv.reader(csvplik)
        for wiersz in czytnik:
            bazadanych.save({'cause': wiersz[3], 'year': wiersz[0], 'country': wiersz[1], 'sex': wiersz[4], 'value': wiersz[5]})


dodajwpis('hlth_cd_asdr_1_Data.csv')
dodajwpis('hlth_cd_asdr_2_Data.csv')
dodajwpis('hlth_cd_asdr_3_Data.csv')
dodajwpis('hlth_cd_asdr_4_Data.csv')
dodajwpis('hlth_cd_asdr_5_Data.csv')
dodajwpis('hlth_cd_asdr_6_Data.csv')